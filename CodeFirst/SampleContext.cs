﻿using System.Data.Entity;
using CodeFirst.Model;

namespace CodeFirst
{
    public class SampleContext : DbContext
    {
        // Имя будущей базы данных можно указать через
        // вызов конструктора базового класса
        public SampleContext() : base("MyInternetShop")
        { }

        // Отражение таблиц базы данных на свойства с типом DbSet

        /// <summary>
        /// Таблица пользователей
        /// </summary>
        public DbSet<Employee> Employees { get; set; }

        /// <summary>
        /// Таблица пользователей
        /// </summary>
        public DbSet<User> Users{ get; set; }

        /// <summary>
        /// Таблица заказов
        /// </summary>
        public DbSet<Order> Orders { get; set; }

        /// <summary>
        /// Таблица товаров
        /// </summary>
        public DbSet<Product> Products { get; set; }

        /// <summary>
        /// Таблица категорий
        /// </summary>
        public DbSet<Category> Categories { get; set; }

        /// <summary>
        /// Таблица картинок
        /// </summary>
        public DbSet<Image> Images { get; set; }
    }
}

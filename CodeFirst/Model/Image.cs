﻿using System.ComponentModel.DataAnnotations;

namespace CodeFirst.Model
{
    /// <summary>
    /// Картинка
    /// </summary>
    public class Image
    {
        /// <summary>
        /// Id картинки.
        /// </summary>
        [Key]
        public int ImageId { get; set; }

        /// <summary>
        /// Описание картинки
        /// </summary>
        [Required]
        [MaxLength(50)]
        public string Description { get; set; }

        /// <summary>
        /// Картинка
        /// </summary>
        [Required]
        public byte[] Content { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace CodeFirst.Model
{
    /// <summary>
    /// Сотрудник
    /// </summary>
    public class Employee
    {
        /// <summary>
        /// Ид сотрудника
        /// </summary>
        [Key]
        public int EmplooyeeId { get; set; }

        /// <summary>
        /// Логин
        /// </summary>
        [Required]
        [MaxLength(20)]
        public string Login { get; set; }

        /// <summary>
        /// Пароль
        /// </summary>
        [Required]
        public int Password { get; set; }

        /// <summary>
        /// Должность
        /// </summary>
        [Required]
        public int Post { get; set; }

        /// <summary>
        /// Имя
        /// </summary>
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
    }
}

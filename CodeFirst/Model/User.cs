﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CodeFirst.Model
{
    /// <summary>
    /// Пользователь
    /// </summary>
    public class User
    {
        /// <summary>
        /// Ид пользователя
        /// </summary>
        [Key]
        public int UserId { get; set; }

        /// <summary>
        /// Логин
        /// </summary>
        [Required]
        [MaxLength(20)]
        public string Login { get; set; }

        /// <summary>
        /// Пароль
        /// </summary>
        [Required]
        public int Password { get; set; }

        /// <summary>
        /// Имя
        /// </summary>
        [Required]
        [MaxLength(20)]
        public string Name { get; set; }

        /// <summary>
        /// E-mail
        /// </summary>
        [Required]
        [MaxLength(50)]
        public string Email { get; set; }

        /// <summary>
        /// Номер телефона
        /// </summary>
        [Required]
        [MaxLength(12)]
        public string PhoneNumber { get; set; }

        // Ссылка на заказы
        public virtual List<Order> Orders { get; set; }
    }
}

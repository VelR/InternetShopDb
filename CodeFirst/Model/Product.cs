﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CodeFirst.Model
{
    /// <summary>
    /// Товар
    /// </summary>
    public class Product
    {
        /// <summary>
        /// Ид продукта
        /// </summary>
        [Key]
        public int ProductId { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        [Required]
        [MaxLength(30)]
        public string Name { get; set; }

        /// <summary>
        /// Категория
        /// </summary>
        [Required]
        public Category Category { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Закупочная цена
        /// </summary>
        [Required]
        public int PurchasePrice { get; set; }

        /// <summary>
        /// Картинки
        /// </summary>
        public virtual List<Image> Images { get; set; }
    }
}

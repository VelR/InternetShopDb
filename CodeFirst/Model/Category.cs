﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeFirst.Model
{
    /// <summary>
    /// Компания доставки
    /// </summary>
    public class Category
    {
        /// <summary>
        /// Ид заказа
        /// </summary>
        [Key]
        public int CategoryId { get; set; }

        /// <summary>
        /// Наименование категории
        /// </summary>
        [Required]
        [MaxLength(30)]
        public string Name { get; set; }
    }
}

﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CodeFirst.Model
{
    /// <summary>
    /// Заказ
    /// </summary>
    public class Order
    {
        /// <summary>
        /// Ид заказа
        /// </summary>
        [Key]
        public int OrderId { get; set; }

        /// <summary>
        /// Ссылка на продукт
        /// (Один ко многим)
        /// </summary>
        [Required]
        public Product Product { get; set; }

        /// <summary>
        /// Количество
        /// </summary>
        [Required]
        [Range(0,100)]
        public int Quantity { get; set; }

        /// <summary>
        /// Дата покупки
        /// </summary>
        [Required]
        public DateTime PurchaseDate { get; set; }

        /// <summary>
        /// Ссылка на покупателя
        /// (Один ко многим)
        /// </summary>
        [Required]
        public User User { get; set; }
    }
}

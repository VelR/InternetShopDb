﻿using CodeFirst.Model;
using InternetShop.Common;
using InternetShop.Services;
using InternetShop.View.AdminWindows;
using System;
using System.Windows;

namespace InternetShop.ViewModel.AdminViewModelComponents
{
    /// <summary>
    /// ViewModel окна регистрации.
    /// </summary>
    class EmployeeViewModel : DependencyObject
    {
        #region Поля

        /// <summary>
        /// Вьюха
        /// </summary>
        private EmployeeView _view;

        /// <summary>
        /// Режим редактирования
        /// </summary>
        private Employee _isEditableEmployee;

        #endregion

        #region Свойства зависимостей


        /// <summary>
        /// Текст кнопки.
        /// </summary>
        public string ActionButtonText
        {
            get { return (string)GetValue(ActionButtonTextProperty); }
            set { SetValue(ActionButtonTextProperty, value); }
        }

        public static readonly DependencyProperty ActionButtonTextProperty =
            DependencyProperty.Register("ActionButtonText", typeof(string), typeof(EmployeeViewModel), new PropertyMetadata("Добавить"));



        /// <summary>
        /// Имя.
        /// </summary>
        public string Name
        {
            get { return (string)GetValue(NameProperty); }
            set { SetValue(NameProperty, value); }
        }

        public static readonly DependencyProperty NameProperty =
            DependencyProperty.Register("Name", typeof(string), typeof(EmployeeViewModel), new PropertyMetadata(string.Empty));


        /// <summary>
        /// Логин.
        /// </summary>
        public string Login
        {
            get { return (string)GetValue(LoginProperty); }
            set { SetValue(LoginProperty, value); }
        }

        public static readonly DependencyProperty LoginProperty =
            DependencyProperty.Register("Login", typeof(string), typeof(EmployeeViewModel), new PropertyMetadata(string.Empty));


        /// <summary>
        /// Пароль.
        /// </summary>
        public string Password
        {
            get { return (string)GetValue(PasswordProperty); }
            set { SetValue(PasswordProperty, value); }
        }

        public static readonly DependencyProperty PasswordProperty =
            DependencyProperty.Register("Password", typeof(string), typeof(EmployeeViewModel), new PropertyMetadata(string.Empty));


        /// <summary>
        /// Должность
        /// </summary>
        public int Post
        {
            get { return (int)GetValue(PostProperty); }
            set { SetValue(PostProperty, value); }
        }

        public static readonly DependencyProperty PostProperty =
            DependencyProperty.Register("Post", typeof(int), typeof(EmployeeViewModel), new PropertyMetadata(0));

        #endregion

        #region Конструктор

        /// <summary>
        /// Конструктор ViewModel окна Регистрации.
        /// </summary>
        public EmployeeViewModel(EmployeeView view, Employee employee = null)
        {
            _view = view;
            _isEditableEmployee = employee;
            if (_isEditableEmployee != null)
            {
                _view.Title = "Редактирование пользователя.";
                ActionButtonText = "Редактировать";
                Name = employee.Name;
                Login = employee.Login;
                Post = employee.Post;
            }
            else
            {
                _view.Title = "Добавить пользователя.";
                ActionButtonText = "Добавить";
            }
        }

        #endregion

        #region Методы

        /// <summary>
        /// Проверка на валидацию полей.
        /// </summary>
        private bool IsValid() => !string.IsNullOrWhiteSpace(Name)
                && !string.IsNullOrWhiteSpace(Login)
                && !string.IsNullOrWhiteSpace(Password);

        /// <summary>
        /// Регистрация пользователя.
        /// </summary>
        private void Registration()
        {
            try
            {
                if (!DataBaseService.HasEmployee(Login))
                {
                    AddEmployee();
                    _view.DialogResult = true;
                }
                else
                {
                    MessageBox.Show($"Пользователь с таким логином уже существует.", "Ошибка при регистрации");
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Ошибка");
            }
        }

        /// <summary>
        /// Добавить пользователя.
        /// </summary>
        private void AddEmployee()
        {
            if (!DataBaseService.HasLogin(Login))
            {
                var employee = new Employee
                {
                    Name = Name,
                    Login = Login,
                    Password = AppService.GetPasswordHash(Login, Password),
                    Post = Post,
                };
                DataBaseService.AddEmployee(employee);
                _view.DialogResult = true;
            }
            else
            {
                MessageBox.Show("Пользователь с таким логином уже существует, используйте другой логин.");
            }
        }

        /// <summary>
        /// Редактировать пользователя
        /// </summary>
        private void EditEmployee()
        {
            if (Login != _isEditableEmployee.Login)
            {
                if (!DataBaseService.HasLogin(Login))
                {
                    _isEditableEmployee.Login = Login;
                }
                else
                {
                    MessageBox.Show("Пользователь с таким логином уже существует, используйте другой логин.");
                    return;
                }
            }
            _isEditableEmployee.Password = AppService.GetPasswordHash(Login, Password);
            _isEditableEmployee.Post = Post;
            _isEditableEmployee.Name = Name;

            DataBaseService.SaveChanges();
            _view.DialogResult = true;
        }

        #endregion

        #region Команды

        /// <summary>
        /// Команда на кнопку действия.
        /// </summary>
        public RelayCommand ActionCommand
        {
            get
            {
                return new RelayCommand(obj =>
                {
                    if (_isEditableEmployee != null)
                    {
                        EditEmployee();
                    }
                    else
                    {
                        AddEmployee();
                    }
                }, o => IsValid());
            }
        }



        /// <summary>
        /// Команда на линк "Создать новый аккаунт".
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                return new RelayCommand(obj =>
                {
                    _view.DialogResult = false;
                });
            }
        }

        #endregion
    }
}

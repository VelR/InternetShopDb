﻿using CodeFirst.Model;
using InternetShop.Common;
using InternetShop.Services;
using InternetShop.View.AdminWindows;
using System;
using System.Windows;

namespace InternetShop.ViewModel.AdminViewModelComponents
{
    /// <summary>
    /// ViewModel окна регистрации.
    /// </summary>
    class UserViewModel : DependencyObject
    {
        #region Поля

        /// <summary>
        /// Вьюха
        /// </summary>
        private UserView _view;

        /// <summary>
        /// Режим редактирования
        /// </summary>
        private User _isEditableUser;

        #endregion

        #region Свойства зависимостей


        /// <summary>
        /// Текст кнопки.
        /// </summary>
        public string ActionButtonText
        {
            get { return (string)GetValue(ActionButtonTextProperty); }
            set { SetValue(ActionButtonTextProperty, value); }
        }

        public static readonly DependencyProperty ActionButtonTextProperty =
            DependencyProperty.Register("ActionButtonText", typeof(string), typeof(UserViewModel), new PropertyMetadata("Добавить"));



        /// <summary>
        /// Имя.
        /// </summary>
        public string Name
        {
            get { return (string)GetValue(NameProperty); }
            set { SetValue(NameProperty, value); }
        }

        public static readonly DependencyProperty NameProperty =
            DependencyProperty.Register("Name", typeof(string), typeof(UsersViewModel), new PropertyMetadata(string.Empty));


        /// <summary>
        /// Логин.
        /// </summary>
        public string Login
        {
            get { return (string)GetValue(LoginProperty); }
            set { SetValue(LoginProperty, value); }
        }

        public static readonly DependencyProperty LoginProperty =
            DependencyProperty.Register("Login", typeof(string), typeof(UsersViewModel), new PropertyMetadata(string.Empty));


        /// <summary>
        /// Пароль.
        /// </summary>
        public string Password
        {
            get { return (string)GetValue(PasswordProperty); }
            set { SetValue(PasswordProperty, value); }
        }

        public static readonly DependencyProperty PasswordProperty =
            DependencyProperty.Register("Password", typeof(string), typeof(UsersViewModel), new PropertyMetadata(string.Empty));


        /// <summary>
        /// E-mail.
        /// </summary>
        public string Email
        {
            get { return (string)GetValue(EmailProperty); }
            set { SetValue(EmailProperty, value); }
        }

        public static readonly DependencyProperty EmailProperty =
            DependencyProperty.Register("Email", typeof(string), typeof(UsersViewModel), new PropertyMetadata(string.Empty));


        /// <summary>
        /// Номер телефона.
        /// </summary>
        public string Phone
        {
            get { return (string)GetValue(PhoneProperty); }
            set { SetValue(PhoneProperty, value); }
        }

        public static readonly DependencyProperty PhoneProperty =
            DependencyProperty.Register("Phone", typeof(string), typeof(UsersViewModel), new PropertyMetadata(string.Empty));

        #endregion

        #region Конструктор

        /// <summary>
        /// Конструктор ViewModel окна Регистрации.
        /// </summary>
        public UserViewModel(UserView view, User user = null)
        {
            _view = view;
            _isEditableUser = user;
            if (_isEditableUser != null)
            {
                _view.Title = "Редактирование пользователя.";
                ActionButtonText = "Редактировать";
                Name = user.Name;
                Login = user.Login;
                Email = user.Email;
                Phone = user.PhoneNumber;
            }
            else
            {
                _view.Title = "Добавить пользователя.";
                ActionButtonText = "Добавить";
            }
        }

        #endregion

        #region Методы

        /// <summary>
        /// Проверка на валидацию полей.
        /// </summary>
        private bool IsValid() => !string.IsNullOrWhiteSpace(Name)
                && !string.IsNullOrWhiteSpace(Login)
                && !string.IsNullOrWhiteSpace(Email)
                && !string.IsNullOrWhiteSpace(Phone) && Phone.Length == 12
                && !string.IsNullOrWhiteSpace(Password);

        /// <summary>
        /// Регистрация пользователя.
        /// </summary>
        private void Registration()
        {
            try
            {
                if (!DataBaseService.HasLogin(Login))
                {
                    AddUser();
                    MessageBox.Show($"{Name}, вы успешно зарегестрировались!", "Успешная регистрация");
                    _view.DialogResult = true;
                }
                else
                {
                    MessageBox.Show($"Пользователь с таким логином уже существует.", "Ошибка при регистрации");
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Ошибка");
            }
        }

        /// <summary>
        /// Добавить пользователя.
        /// </summary>
        private void AddUser()
        {
            if(!DataBaseService.HasLogin(Login))
            {
                var user = new User
                {
                    Name = Name,
                    Login = Login,
                    Password = AppService.GetPasswordHash(Login, Password),
                    Email = Email,
                    PhoneNumber = Phone
                };
                DataBaseService.AddUser(user);
                _view.DialogResult = true;
            }
            else
            {
                MessageBox.Show("Пользователь с таким логином уже существует, используйте другой логин.");
            }           
        }

        /// <summary>
        /// Редактировать пользователя
        /// </summary>
        private void EditUser()
        {
            if(Login != _isEditableUser.Login)
            {
                if (!DataBaseService.HasLogin(Login))
                {
                    _isEditableUser.Login = Login;
                }
                else
                {
                    MessageBox.Show("Пользователь с таким логином уже существует, используйте другой логин.");
                    return;
                }
            }
            _isEditableUser.Password = AppService.GetPasswordHash(Login, Password);
            _isEditableUser.Email = Email;
            _isEditableUser.PhoneNumber = Phone;
            _isEditableUser.Name = Name;

            DataBaseService.SaveChanges();
            _view.DialogResult = true;
        }

        #endregion

        #region Команды

        /// <summary>
        /// Команда на кнопку действия.
        /// </summary>
        public RelayCommand ActionCommand
        {
            get
            {
                return new RelayCommand(obj =>
                {
                    if (_isEditableUser != null)
                    {
                        EditUser();
                    }
                    else
                    {
                        AddUser();
                    }
                }, o => IsValid());
            }
        }



        /// <summary>
        /// Команда на линк "Создать новый аккаунт".
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                return new RelayCommand(obj =>
                {
                    _view.DialogResult = false;
                });
            }
        }

        #endregion
    }
}

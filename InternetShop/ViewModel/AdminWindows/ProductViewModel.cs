﻿using CodeFirst.Model;
using InternetShop.Common;
using InternetShop.Services;
using InternetShop.View.AdminWindows;
using System.Collections.Generic;
using System.Windows;

namespace InternetShop.ViewModel.AdminViewModelComponents
{
    /// <summary>
    /// ViewModel окна регистрации.
    /// </summary>
    class ProductViewModel : DependencyObject
    {
        #region Поля

        /// <summary>
        /// Вьюха
        /// </summary>
        private ProductView _view;

        /// <summary>
        /// Режим редактирования
        /// </summary>
        private Product _isEditableProduct;

        #endregion

        #region Свойства зависимостей


        /// <summary>
        /// Текст кнопки.
        /// </summary>
        public string ActionButtonText
        {
            get { return (string)GetValue(ActionButtonTextProperty); }
            set { SetValue(ActionButtonTextProperty, value); }
        }

        public static readonly DependencyProperty ActionButtonTextProperty =
            DependencyProperty.Register("ActionButtonText", typeof(string), typeof(ProductViewModel), new PropertyMetadata("Добавить"));


        /// <summary>
        /// Выбранная категория
        /// </summary>
        public Category Category
        {
            get { return (Category)GetValue(CategoryProperty); }
            set { SetValue(CategoryProperty, value); }
        }

        public static readonly DependencyProperty CategoryProperty =
            DependencyProperty.Register("Category", typeof(Category), typeof(ProductViewModel), new PropertyMetadata(null));


        /// <summary>
        /// Категории
        /// </summary>
        public List<Category> Categories
        {
            get { return (List<Category>)GetValue(CategoriesProperty); }
            set { SetValue(CategoriesProperty, value); }
        }

        public static readonly DependencyProperty CategoriesProperty =
            DependencyProperty.Register("Categories", typeof(List<Category>), typeof(ProductViewModel), new PropertyMetadata(DataBaseService.GetCategories()));


        /// <summary>
        /// Наименование.
        /// </summary>
        public string Name
        {
            get { return (string)GetValue(NameProperty); }
            set { SetValue(NameProperty, value); }
        }

        public static readonly DependencyProperty NameProperty =
            DependencyProperty.Register("Name", typeof(string), typeof(ProductViewModel), new PropertyMetadata(string.Empty));


        /// <summary>
        /// Описание.
        /// </summary>
        public string Description
        {
            get { return (string)GetValue(DescriptionProperty); }
            set { SetValue(DescriptionProperty, value); }
        }

        public static readonly DependencyProperty DescriptionProperty =
            DependencyProperty.Register("Description", typeof(string), typeof(ProductViewModel), new PropertyMetadata(string.Empty));


        /// <summary>
        /// Закупочная цена.
        /// </summary>
        public int PurchasePrice
        {
            get { return (int)GetValue(PurchasePriceProperty); }
            set { SetValue(PurchasePriceProperty, value); }
        }

        public static readonly DependencyProperty PurchasePriceProperty =
            DependencyProperty.Register("PurchasePrice", typeof(int), typeof(ProductViewModel), new PropertyMetadata(0));



        #endregion

        #region Конструктор

        /// <summary>
        /// Конструктор ViewModel окна товаров.
        /// </summary>
        public ProductViewModel(ProductView view, Product product = null)
        {
            _view = view;
            _isEditableProduct = product;
            if (_isEditableProduct != null)
            {
                _view.Title = "Редактирование товар.";
                ActionButtonText = "Редактировать";
                Name = product.Name;
                Description = product.Description;
                Category = product.Category;
                PurchasePrice = product.PurchasePrice;
            }
            else
            {
                _view.Title = "Добавить товар.";
                ActionButtonText = "Добавить";
            }
        }

        #endregion

        #region Методы

        /// <summary>
        /// Проверка на валидацию полей.
        /// </summary>
        private bool IsValid() => !string.IsNullOrWhiteSpace(Name)
                && !string.IsNullOrWhiteSpace(Description)
                && Category != null
                && PurchasePrice != 0;

        /// <summary>
        /// Добавить товара.
        /// </summary>
        private void AddProduct()
        {
            var product = new Product
            {
                Name = Name,
                Description = Description,
                Category = Category,
                PurchasePrice = PurchasePrice
            };
            DataBaseService.AddProduct(product);
            _view.DialogResult = true;
        }

        /// <summary>
        /// Редактировать пользователя
        /// </summary>
        private void EditProduct()
        {
            _isEditableProduct.Category = Category;
            _isEditableProduct.PurchasePrice = PurchasePrice;
            _isEditableProduct.Description = Description;
            _isEditableProduct.Name = Name;

            DataBaseService.SaveChanges();
            _view.DialogResult = true;
        }

        #endregion

        #region Команды

        /// <summary>
        /// Команда на кнопку действия.
        /// </summary>
        public RelayCommand ActionCommand
        {
            get
            {
                return new RelayCommand(obj =>
                {
                    if (_isEditableProduct != null)
                    {
                        EditProduct();
                    }
                    else
                    {
                        AddProduct();
                    }
                }, o => IsValid());
            }
        }



        /// <summary>
        /// Команда на линк "Создать новый аккаунт".
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                return new RelayCommand(obj =>
                {
                    _view.DialogResult = false;
                });
            }
        }

        #endregion
    }
}

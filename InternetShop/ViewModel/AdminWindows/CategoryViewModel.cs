﻿using CodeFirst.Model;
using InternetShop.Common;
using InternetShop.Services;
using InternetShop.View.AdminWindows;
using System;
using System.Windows;

namespace InternetShop.ViewModel.AdminViewModelComponents
{
    /// <summary>
    /// ViewModel окна регистрации.
    /// </summary>
    class CategoryViewModel : DependencyObject
    {
        #region Поля

        /// <summary>
        /// Вьюха
        /// </summary>
        private CategoryView _view;

        #endregion

        #region Свойства зависимостей

        /// <summary>
        /// Имя.
        /// </summary>
        public string Name
        {
            get { return (string)GetValue(NameProperty); }
            set { SetValue(NameProperty, value); }
        }

        public static readonly DependencyProperty NameProperty =
            DependencyProperty.Register("Name", typeof(string), typeof(CategoryViewModel), new PropertyMetadata(string.Empty));

        #endregion

        #region Конструктор

        /// <summary>
        /// Конструктор ViewModel окна добавления категории.
        /// </summary>
        public CategoryViewModel(CategoryView view)
        {
            _view = view;
        }

        #endregion

        #region Методы

        /// <summary>
        /// Проверка на валидацию полей.
        /// </summary>
        private bool IsValid() => !string.IsNullOrWhiteSpace(Name);

        /// <summary>
        /// Добавление категории.
        /// </summary>
        private void AddCategory()
        {
            if(!DataBaseService.HasCategory(Name))
            {
                var category = new Category
                {
                    Name = Name
                };
                DataBaseService.AddCategory(category);
                MessageBox.Show("Категория добавлена в базу данных.", "Уведомление");
                _view.DialogResult = true;
            }
            else
            {
                MessageBox.Show("Такая категория уже существует, используйте другой наименование.");
            }           
        }

        #endregion

        #region Команды

        /// <summary>
        /// Команда на кнопку действия.
        /// </summary>
        public RelayCommand AddCommand
        {
            get
            {
                return new RelayCommand(obj =>
                {
                    AddCategory();
                }, o => IsValid());
            }
        }

        /// <summary>
        /// Команда на линк "Создать новый аккаунт".
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                return new RelayCommand(obj =>
                {
                    _view.DialogResult = false;
                });
            }
        }

        #endregion
    }
}

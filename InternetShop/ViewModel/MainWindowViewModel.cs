﻿using CodeFirst.Model;
using InternetShop.Common;
using InternetShop.View.AdminViewComponents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace InternetShop.ViewModel
{
    /// <summary>
    /// ViewModel для основного окна.
    /// </summary>
    public class MainWindowViewModel : DependencyObject
    {
        #region Поля

        /// <summary>
        /// Авторизованный пользователь.
        /// </summary>
        private User _user;

        /// <summary>
        /// Вьюха главного окна.
        /// </summary>
        private MainWindow _view;

        #endregion

        #region Свойства зависимостей
        
        /// <summary>
        /// Авторизованный пользователь.
        /// </summary>
        public User User
        {
            get { return (User)GetValue(UserProperty); }
            set { SetValue(UserProperty, value); }
        }

        public static readonly DependencyProperty UserProperty =
            DependencyProperty.Register("User", typeof(User), typeof(MainWindowViewModel), new PropertyMetadata(null, UserPropertyChanged));

        /// <summary>
        /// Обработчик изменения пользователя.
        /// </summary>
        private static void UserPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {            
            if(d is MainWindowViewModel)
            {
                var vm = d as MainWindowViewModel;
                vm._view.Title = $"InternetShopApplication. Пользователь: {vm.User.Login}";
            }
        }

        #endregion

        #region Конструктор

        /// <summary>
        /// Конструктор основного окна.
        /// </summary>
        public MainWindowViewModel(User user, MainWindow view)
        {
            _view = view;
            _user = user;
        }

        #endregion

        #region Команды

        /// <summary>
        /// Команда на кнопку "Сменить пользователя".
        /// </summary>
        public RelayCommand ChangeUser
        {
            get
            {
                return new RelayCommand(obj =>
                {

                });
            }
        }

        #endregion
    }
}

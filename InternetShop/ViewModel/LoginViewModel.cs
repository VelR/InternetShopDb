﻿using System.Linq;
using System.Windows;
using CodeFirst;
using CodeFirst.Model;
using System.Security;
using InternetShop.Common;
using InternetShop.View;
using InternetShop.Services;

namespace InternetShop.ViewModel
{
    /// <summary>
    /// ViewModel для окна авторизации.
    /// </summary>
    public class LoginViewModel : DependencyObject
    {
        #region Поля

        /// <summary>
        /// Вьюха.
        /// </summary>
        private LoginView _view;

        #endregion

        #region Свойства

        /// <summary>
        /// Авторизованный пользователь.
        /// </summary>
        public Employee Employee { get; set; }

        /// <summary>
        /// Пароль вводимый пользователем
        /// </summary>
        public string Password
        {
            get { return (string)GetValue(PasswordProperty); }
            set { SetValue(PasswordProperty, value); }
        }

        public static readonly DependencyProperty PasswordProperty =
            DependencyProperty.Register("Password", typeof(string), typeof(LoginViewModel), new PropertyMetadata(string.Empty));


        /// <summary>
        /// Логин
        /// </summary>
        public string Login
        {
            get => (string)GetValue(LoginProperty);
            set => SetValue(LoginProperty, value);
        }

        public static readonly DependencyProperty LoginProperty =
            DependencyProperty.Register("Login", typeof(string), typeof(LoginViewModel), new PropertyMetadata(string.Empty));

        #endregion

        #region Конструктор

        /// <summary>
        /// Конструктор ViewModel окна авторизации.
        /// </summary>
        public LoginViewModel(LoginView view)
        {
            _view = view;
        }

        #endregion

        #region Методы

        /// <summary>
        /// Авторизация пользователя.
        /// </summary>
        private void Authorization(string login, string userPass)
        {
            var employee = DataBaseService.GetEmployee(login);
            if (employee != null)
            {
                if (AppService.GetPasswordHash(login, userPass) == employee.Password)
                {
                    Employee = employee;
                    _view.DialogResult = true;
                }
                else
                {
                    MessageBox.Show("Вы ввели не правильный пароль, повторите попытку", "Ошибка авторизации");
                }
            }
            else
            {
                MessageBox.Show("Пользователь с таким логином не найден, необходимо зарегистрироваться", "Ошибка авторизации");
            }
        }

        #endregion

        #region Команды

        /// <summary>
        /// Команда на кнопку "Войти".
        /// </summary>
        public RelayCommand LoginCommand
        {
            get
            {
                return new RelayCommand(obj =>
                {
                    Authorization(Login, Password);
                }, o => !string.IsNullOrEmpty(Login) && !string.IsNullOrEmpty(Password));
            }
        }

        /// <summary>
        /// Команда на линк "Создать новый аккаунт".
        /// </summary>
        public RelayCommand CreateCommand
        {
            get
            {
                return new RelayCommand(obj =>
                {
                    var regView = new RegistrView();
                    var regViewModel = new RegistrViewModel(regView);
                    regView.DataContext = regViewModel;
                    regView.ShowDialog();
                });
            }
        }

        #endregion
    }
}

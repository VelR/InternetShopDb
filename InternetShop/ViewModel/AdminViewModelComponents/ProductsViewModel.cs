﻿using CodeFirst.Model;
using InternetShop.Common;
using InternetShop.Services;
using InternetShop.View.AdminWindows;
using System.Collections.Generic;
using System.Windows;

namespace InternetShop.ViewModel.AdminViewModelComponents
{
    class ProductsViewModel : DependencyObject
    {
        #region Свойства зависимостей

        public Product SelectedItem
        {
            get { return (Product)GetValue(SelectedItemProperty); }
            set { SetValue(SelectedItemProperty, value); }
        }

        public static readonly DependencyProperty SelectedItemProperty =
            DependencyProperty.Register("SelectedItem", typeof(Product), typeof(ProductsViewModel), new PropertyMetadata(null));


        public List<Product> Items
        {
            get { return (List<Product>)GetValue(ItemsProperty); }
            set { SetValue(ItemsProperty, value); }
        }

        public static readonly DependencyProperty ItemsProperty =
            DependencyProperty.Register("Items", typeof(List<Product>), typeof(ProductsViewModel), new PropertyMetadata(DataBaseService.GetProducts()));


        #endregion

        #region Методы

        /// <summary>
        /// Обновить itemsource ля таблицы.
        /// </summary>
        private void RefreshItemSource()
        {
            Items = (List<Product>)DataBaseService.GetProducts();
        }

        #endregion

        #region Команды

        /// <summary>
        /// Команда на кнопку "Добавить".
        /// </summary>
        public RelayCommand AddProductCommand
        {
            get
            {
                return new RelayCommand(obj =>
                {
                    var view = new ProductView();
                    var vm = new ProductViewModel(view);
                    view.DataContext = vm;
                    view.ShowDialog();
                    RefreshItemSource();
                });
            }
        }

        /// <summary>
        /// Команда на кнопку "Редактировать".
        /// </summary>
        public RelayCommand EditProductCommand
        {
            get
            {
                return new RelayCommand(obj =>
                {
                    var view = new ProductView();
                    var vm = new ProductViewModel(view, SelectedItem);
                    view.DataContext = vm;
                    view.ShowDialog();
                    RefreshItemSource();
                }, o => SelectedItem != null);
            }
        }

        /// <summary>
        /// Команда на кнопку "Удалить".
        /// </summary>
        public RelayCommand DeleteProductCommand
        {
            get
            {
                return new RelayCommand(obj =>
                {
                    DataBaseService.DeleteProduct(SelectedItem);
                    RefreshItemSource();
                }, o => SelectedItem != null);
            }
        }

        #endregion
    }
}

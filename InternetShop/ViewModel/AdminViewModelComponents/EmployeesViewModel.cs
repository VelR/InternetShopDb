﻿using CodeFirst.Model;
using InternetShop.Common;
using InternetShop.Services;
using InternetShop.View.AdminWindows;
using System.Collections.Generic;
using System.Windows;

namespace InternetShop.ViewModel.AdminViewModelComponents
{
    class EmployeesViewModel : DependencyObject
    {
        #region Свойства зависимсости

        public Employee SelectedItem
        {
            get { return (Employee)GetValue(SelectedItemProperty); }
            set { SetValue(SelectedItemProperty, value); }
        }

        public static readonly DependencyProperty SelectedItemProperty =
            DependencyProperty.Register("SelectedItem", typeof(Employee), typeof(EmployeesViewModel), new PropertyMetadata(null));


        public List<Employee> Items
        {
            get { return (List<Employee>)GetValue(ItemsProperty); }
            set { SetValue(ItemsProperty, value); }
        }

        public static readonly DependencyProperty ItemsProperty =
            DependencyProperty.Register("Items", typeof(List<Employee>), typeof(EmployeesViewModel), new PropertyMetadata(DataBaseService.GetEmployees()));

        #endregion

        #region Методы

        /// <summary>
        /// Обновить itemsource ля таблицы.
        /// </summary>
        private void RefreshItemSource()
        {
            Items = (List<Employee>)DataBaseService.GetEmployees();
        }

        #endregion

        #region Команды

        /// <summary>
        /// Команда на кнопку "Добавить".
        /// </summary>
        public RelayCommand AddEmployeeCommand
        {
            get
            {
                return new RelayCommand(obj =>
                {
                    var view = new EmployeeView();
                    var vm = new EmployeeViewModel(view);
                    view.DataContext = vm;
                    view.ShowDialog();
                    RefreshItemSource();
                });
            }
        }

        /// <summary>
        /// Команда на кнопку "Редактировать".
        /// </summary>
        public RelayCommand EditEmployeeCommand
        {
            get
            {
                return new RelayCommand(obj =>
                {
                    var view = new EmployeeView();
                    var vm = new EmployeeViewModel(view, SelectedItem);
                    view.DataContext = vm;
                    view.ShowDialog();
                    RefreshItemSource();
                }, o => SelectedItem != null);
            }
        }

        /// <summary>
        /// Команда на кнопку "Удалить".
        /// </summary>
        public RelayCommand DeleteEmployeeCommand
        {
            get
            {
                return new RelayCommand(obj =>
                {
                    DataBaseService.DeleteEmployee(SelectedItem);
                    RefreshItemSource();
                }, o => SelectedItem != null);
            }
        }
        
        #endregion
    }
}

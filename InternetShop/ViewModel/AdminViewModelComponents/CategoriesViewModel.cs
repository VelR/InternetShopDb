﻿using CodeFirst.Model;
using InternetShop.Common;
using InternetShop.Services;
using InternetShop.View.AdminWindows;
using System.Collections.Generic;
using System.Windows;

namespace InternetShop.ViewModel.AdminViewModelComponents
{
    class CategoriesViewModel : DependencyObject
    {
        #region Свойства зависимостей

        public Category SelectedItem
        {
            get { return (Category)GetValue(SelectedItemProperty); }
            set { SetValue(SelectedItemProperty, value); }
        }

        public static readonly DependencyProperty SelectedItemProperty =
            DependencyProperty.Register("SelectedItem", typeof(Category), typeof(CategoriesViewModel), new PropertyMetadata(null));


        public List<Category> Items
        {
            get { return (List<Category>)GetValue(ItemsProperty); }
            set { SetValue(ItemsProperty, value); }
        }

        public static readonly DependencyProperty ItemsProperty =
            DependencyProperty.Register("Items", typeof(List<Category>), typeof(CategoriesViewModel), new PropertyMetadata(DataBaseService.GetCategories()));

        #endregion

        #region Методы

        /// <summary>
        /// Обновить itemsource ля таблицы.
        /// </summary>
        private void RefreshItemSource()
        {
            Items = (List<Category>)DataBaseService.GetCategories();
        }

        #endregion

        #region Команды

        /// <summary>
        /// Команда на кнопку "Добавить".
        /// </summary>
        public RelayCommand AddCategoryCommand
        {
            get
            {
                return new RelayCommand(obj =>
                {
                    var view = new CategoryView();
                    var vm = new CategoryViewModel(view);
                    view.DataContext = vm;
                    view.ShowDialog();
                    RefreshItemSource();
                });
            }
        }

        /// <summary>
        /// Команда на кнопку "Удалить".
        /// </summary>
        public RelayCommand DeleteCategoryCommand
        {
            get
            {
                return new RelayCommand(obj =>
                {
                    DataBaseService.DeleteCategory(SelectedItem);
                    RefreshItemSource();
                }, o => SelectedItem != null);
            }
        }

        #endregion
    }
}

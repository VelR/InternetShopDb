﻿using CodeFirst.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace InternetShop.ViewModel.AdminViewModelComponents
{
    class OrdersViewModel : DependencyObject
    {
        public Order SelectedItem
        {
            get { return (Order)GetValue(SelectedItemProperty); }
            set { SetValue(SelectedItemProperty, value); }
        }

        public static readonly DependencyProperty SelectedItemProperty =
            DependencyProperty.Register("SelectedItem", typeof(Order), typeof(OrdersViewModel), new PropertyMetadata(null));


        public List<Order> Items
        {
            get { return (List<Order>)GetValue(ItemsProperty); }
            set { SetValue(ItemsProperty, value); }
        }

        public static readonly DependencyProperty ItemsProperty =
            DependencyProperty.Register("Items", typeof(List<Order>), typeof(OrdersViewModel), new PropertyMetadata(null));        
    }
}

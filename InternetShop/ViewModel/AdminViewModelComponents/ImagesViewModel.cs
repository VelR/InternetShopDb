﻿using CodeFirst.Model;
using InternetShop.Common;
using InternetShop.Services;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;

namespace InternetShop.ViewModel.AdminViewModelComponents
{
    class ImagesViewModel : DependencyObject
    {
        #region Свойства зависимостей

        public Image SelectedItem
        {
            get { return (Image)GetValue(SelectedItemProperty); }
            set { SetValue(SelectedItemProperty, value); }
        }

        public static readonly DependencyProperty SelectedItemProperty =
            DependencyProperty.Register("SelectedItem", typeof(Image), typeof(ImagesViewModel), new PropertyMetadata(null));


        public List<Image> Items
        {
            get { return (List<Image>)GetValue(ItemsProperty); }
            set { SetValue(ItemsProperty, value); }
        }

        public static readonly DependencyProperty ItemsProperty =
            DependencyProperty.Register("Items", typeof(List<Image>), typeof(ImagesViewModel), new PropertyMetadata(DataBaseService.GetImages()));

        public Product ProductItem
        {
            get { return (Product)GetValue(ProductItemProperty); }
            set { SetValue(ProductItemProperty, value); }
        }

        public static readonly DependencyProperty ProductItemProperty =
            DependencyProperty.Register("ProductItem", typeof(Product), typeof(ImagesViewModel), new PropertyMetadata(null));


        public List<Product> ProductsItems
        {
            get { return (List<Product>)GetValue(ProductsItemsProperty); }
            set { SetValue(ProductsItemsProperty, value); }
        }

        public static readonly DependencyProperty ProductsItemsProperty =
            DependencyProperty.Register("ProductsItems", typeof(List<Product>), typeof(ImagesViewModel), new PropertyMetadata(DataBaseService.GetProducts()));

        #endregion

        #region Методы

        /// <summary>
        /// Обновить itemsource ля таблицы.
        /// </summary>
        private void RefreshItemSource()
        {
            Items = (List<Image>)DataBaseService.GetImages();
        }

        public static BitmapImage BitmapImageFromBytes(byte[] bytes)
        {
            BitmapImage image = null;
            MemoryStream stream = null;
            try
            {
                stream = new MemoryStream(bytes);
                stream.Seek(0, SeekOrigin.Begin);
                System.Drawing.Image img = System.Drawing.Image.FromStream(stream);
                image = new BitmapImage();
                image.BeginInit();
                MemoryStream ms = new MemoryStream();
                img.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                ms.Seek(0, SeekOrigin.Begin);
                image.StreamSource = ms;
                image.StreamSource.Seek(0, SeekOrigin.Begin);
                image.EndInit();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                stream.Close();
                stream.Dispose();
            }
            return image;
        }

        private Image GetNewImage(OpenFileDialog openFileDialog)
        {          

            var img = System.Drawing.Image.FromFile(openFileDialog.FileName);
            byte[] arr;
            using (MemoryStream ms = new MemoryStream())
            {
                img.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                arr = ms.ToArray();
            }
            var image = new Image()
            {
                Description = openFileDialog.SafeFileName,
                Content = arr
            };
            return image;
        }

        #endregion

        #region Команды

        /// <summary>
        /// Команда на кнопку "Добавить".
        /// </summary>
        public RelayCommand AddImageCommand
        {
            get
            {
                return new RelayCommand(obj =>
                {
                    var openFileDialog = new OpenFileDialog();
                    if (openFileDialog.ShowDialog() == true)
                    {
                        var image = GetNewImage(openFileDialog);
                        if(!DataBaseService.HasImage(image.Description))
                        {
                            DataBaseService.AddImage(image);
                            MessageBox.Show("Картинка успешно добавлена в бд", "Успешное добавление");
                            RefreshItemSource();
                        }
                        else
                        {
                            MessageBox.Show("Картинка с таким описанием уже существует в БД", "Ошибка при добавлении");
                        }
                    }
                });
            }
        }

        /// <summary>
        /// Команда на кнопку "Редактировать".
        /// </summary>
        public RelayCommand AddImageToProductCommand
        {
            get
            {
                return new RelayCommand(obj =>
                {
                    DataBaseService.AddImageToProduct(ProductItem, SelectedItem);
                }, o => SelectedItem != null && ProductItem != null);
            }
        }

        /// <summary>
        /// Команда на кнопку "Удалить".
        /// </summary>
        public RelayCommand DeleteImageCommand
        {
            get
            {
                return new RelayCommand(obj =>
                {
                    DataBaseService.DeleteImage(SelectedItem);
                    RefreshItemSource();
                }, o => SelectedItem != null);
            }
        }

        #endregion
    }
}

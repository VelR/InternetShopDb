﻿using CodeFirst.Model;
using InternetShop.Common;
using InternetShop.Services;
using InternetShop.View.AdminWindows;
using System.Collections.Generic;
using System.Windows;

namespace InternetShop.ViewModel.AdminViewModelComponents
{
    class UsersViewModel : DependencyObject
    {
        #region Свойства зависимсости

        public User SelectedItem
        {
            get { return (User)GetValue(SelectedItemProperty); }
            set { SetValue(SelectedItemProperty, value); }
        }

        public static readonly DependencyProperty SelectedItemProperty =
            DependencyProperty.Register("SelectedItem", typeof(User), typeof(UsersViewModel), new PropertyMetadata(null));


        public List<User> Items
        {
            get { return (List<User>)GetValue(ItemsProperty); }
            set { SetValue(ItemsProperty, value); }
        }

        public static readonly DependencyProperty ItemsProperty =
            DependencyProperty.Register("Items", typeof(List<User>), typeof(UsersViewModel), new PropertyMetadata(DataBaseService.GetUsers()));

        #endregion

        #region Методы

        /// <summary>
        /// Обновить itemsource ля таблицы.
        /// </summary>
        private void RefreshItemSource()
        {
            Items = (List<User>)DataBaseService.GetUsers();
        }

        #endregion

        #region Команды

        /// <summary>
        /// Команда на кнопку "Добавить".
        /// </summary>
        public RelayCommand AddUserCommand
        {
            get
            {
                return new RelayCommand(obj =>
                {
                    var view = new UserView();
                    var vm = new UserViewModel(view);
                    view.DataContext = vm;
                    view.ShowDialog();
                    RefreshItemSource();
                });
            }
        }

        /// <summary>
        /// Команда на кнопку "Редактировать".
        /// </summary>
        public RelayCommand EditUserCommand
        {
            get
            {
                return new RelayCommand(obj =>
                {
                    var view = new UserView();
                    var vm = new UserViewModel(view, SelectedItem);
                    view.DataContext = vm;
                    view.ShowDialog();
                    RefreshItemSource();
                }, o => SelectedItem != null);
            }
        }


        /// <summary>
        /// Команда на кнопку "Удалить".
        /// </summary>
        public RelayCommand DeleteUserCommand
        {
            get
            {
                return new RelayCommand(obj =>
                {
                    DataBaseService.DeleteUser(SelectedItem);
                    RefreshItemSource();
                }, o => SelectedItem != null);
            }
        }
        
        #endregion
    }
}

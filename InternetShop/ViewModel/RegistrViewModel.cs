﻿using CodeFirst;
using CodeFirst.Model;
using InternetShop.Common;
using InternetShop.Services;
using InternetShop.View;
using System;
using System.Windows;

namespace InternetShop.ViewModel
{
    /// <summary>
    /// ViewModel окна регистрации.
    /// </summary>
    class RegistrViewModel: DependencyObject
    {
        #region Поля

        /// <summary>
        /// Вьюха.
        /// </summary>
        private RegistrView _view;

        #endregion
        
        #region Свойства зависимостей

        /// <summary>
        /// Имя.
        /// </summary>
        public string Name
        {
            get { return (string)GetValue(NameProperty); }
            set { SetValue(NameProperty, value); }
        }

        public static readonly DependencyProperty NameProperty =
            DependencyProperty.Register("Name", typeof(string), typeof(RegistrViewModel), new PropertyMetadata(string.Empty));


        /// <summary>
        /// Логин.
        /// </summary>
        public string Login
        {
            get { return (string)GetValue(LoginProperty); }
            set { SetValue(LoginProperty, value); }
        }

        public static readonly DependencyProperty LoginProperty =
            DependencyProperty.Register("Login", typeof(string), typeof(RegistrViewModel), new PropertyMetadata(string.Empty));


        /// <summary>
        /// Пароль.
        /// </summary>
        public string Password
        {
            get { return (string)GetValue(PasswordProperty); }
            set { SetValue(PasswordProperty, value); }
        }

        public static readonly DependencyProperty PasswordProperty =
            DependencyProperty.Register("Password", typeof(string), typeof(RegistrViewModel), new PropertyMetadata(string.Empty));


        /// <summary>
        /// Номер телефона.
        /// </summary>
        public string Phone
        {
            get { return (string)GetValue(PhoneProperty); }
            set { SetValue(PhoneProperty, value); }
        }

        public static readonly DependencyProperty PhoneProperty =
            DependencyProperty.Register("Phone", typeof(string), typeof(RegistrViewModel), new PropertyMetadata(string.Empty));
        
        #endregion

        #region Конструктор

        /// <summary>
        /// Конструктор ViewModel окна Регистрации.
        /// </summary>
        public RegistrViewModel(RegistrView view)
        {
            _view = view;
        }

        #endregion

        #region Методы

        /// <summary>
        /// Проверка на валидацию полей.
        /// </summary>
        private bool IsValid() => !string.IsNullOrWhiteSpace(Name)
                && !string.IsNullOrWhiteSpace(Login)
                && !string.IsNullOrWhiteSpace(Password);

        /// <summary>
        /// Регистрация пользователя.
        /// </summary>
        private void Registration()
        {
            try
            {
                if (!DataBaseService.HasEmployee(Login))
                {
                    AddEmployee();
                    MessageBox.Show($"{Name}, вы успешно зарегестрировались!", "Успешная регистрация");
                    _view.DialogResult = true;
                }
                else
                {
                    MessageBox.Show($"Пользователь с таким логином уже существует.", "Ошибка при регистрации");
                }                
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Ошибка");
            }
        }

        /// <summary>
        /// Добавить пользователя.
        /// </summary>
        private void AddEmployee()
        {
            var employee = new Employee
            {
                Name = Name,
                Login = Login,
                Password = AppService.GetPasswordHash(Login, Password),
                Post = 1
            };

            DataBaseService.AddEmployee(employee);
        }

        #endregion

        #region Команды

        /// <summary>
        /// Команда на кнопку "Войти".
        /// </summary>
        public RelayCommand RegisterCommand
        {
            get
            {
                return new RelayCommand(obj =>
                {
                    Registration();
                }, o => IsValid());
            }
        }       

        /// <summary>
        /// Команда на линк "Создать новый аккаунт".
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                return new RelayCommand(obj =>
                {
                    _view.DialogResult = false;
                });
            }
        }

        #endregion
    }
}

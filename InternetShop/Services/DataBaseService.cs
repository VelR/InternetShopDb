﻿using CodeFirst;
using CodeFirst.Model;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Windows;

namespace InternetShop.Services
{
    public class DataBaseService
    {
        #region Поля

        /// <summary>
        /// Контекст БД.
        /// </summary>
        private static SampleContext _context;

        #endregion

        #region Конструктор

        /// <summary>
        /// Конструктор сервиса.
        /// </summary>
        static DataBaseService()
        {
            //Database.SetInitializer(new DropCreateDatabaseIfModelChanges<SampleContext>());
            _context = GetNewContext();
        }

        #endregion

        #region AdminView

        /// <summary>
        /// Возвращает список пользователей.
        /// </summary>
        internal static IEnumerable<User> GetUsers()
        {
            return _context.Users.ToList();
        }

        /// <summary>
        /// Возвращает список картинок.
        /// </summary>
        internal static IEnumerable<Image> GetImages()
        {
            return _context.Images.ToList();
        }

        /// <summary>
        /// Возвращает список категорий.
        /// </summary>
        internal static IEnumerable<Category> GetCategories()
        {
            return _context.Categories.ToList();
        }

        /// <summary>
        /// Возвращает список заказов.
        /// </summary>
        internal static IEnumerable<Order> GetOrders()
        {
            return _context.Orders.ToList();
        }

        /// <summary>
        /// Возвращает список продуктов.
        /// </summary>
        internal static IEnumerable<Product> GetProducts()
        {
            return _context.Products.ToList();
        }

        #endregion

        #region Employee

        /// <summary>
        /// Возвращает список пользователей.
        /// </summary>
        internal static IEnumerable<Employee> GetEmployees()
        {
            return _context.Employees.ToList();
        }

        /// <summary>
        /// Есть ли такой логин в бд.
        /// </summary>
        public static bool HasEmployee(string login)
        {
            return GetEmployee(login) != null;
        }

        /// <summary>
        /// Возвращает Employee по Login.
        /// </summary>
        public static Employee GetEmployee(string login)
        {
            return GetNewContext()?.Employees.FirstOrDefault(u => u.Login == login);
        }

        /// <summary>
        /// Добавить сотрудника
        /// </summary>
        public static void AddEmployee(Employee employee)
        {
            // Добавить пользователя.
            _context.Employees.Add(employee);

            // Сохранить изменения.
            SaveChanges();
        }

        /// <summary>
        /// Удалить пользователя.
        /// </summary>
        public static void DeleteEmployee(Employee employee)
        {
            _context.Employees.Remove(employee);
            SaveChanges();
        }

        #endregion

        #region User

        /// <summary>
        /// Есть ли такой логин в бд.
        /// </summary>
        public static bool HasLogin(string login)
        {
            return GetUser(login) != null;
        }

        /// <summary>
        /// Возвращает User по Login.
        /// </summary>
        public static User GetUser(string login)
        {
            return GetNewContext()?.Users.FirstOrDefault(u => u.Login == login);
        }

        /// <summary>
        /// Добавить пользователя
        /// </summary>
        public static void AddUser(User user)
        {
            // Добавить пользователя.
            _context.Users.Add(user);

            // Сохранить изменения.
            SaveChanges();
        }
        
        /// <summary>
        /// Удалить пользователя.
        /// </summary>
        public static void DeleteUser(User user)
        {
            _context.Users.Remove(user);
            SaveChanges();
        }

        #endregion

        #region Product

        /// <summary>
        /// Добавить товар
        /// </summary>
        public static void AddProduct(Product product)
        {
            // Добавить пользователя.
            _context.Products.Add(product);

            // Сохранить изменения.
            SaveChanges();
        }

        /// <summary>
        /// Добавить картинку к товару
        /// </summary>
        public static void AddImageToProduct(Product product, Image image)
        {
            // Добавить пользователя.
            product.Images.Add(image);

            // Сохранить изменения.
            SaveChanges();
        }

        /// <summary>
        /// Удалить товар.
        /// </summary>
        public static void DeleteProduct(Product product)
        {
            _context.Products.Remove(product);
            SaveChanges();
        }        

        #endregion

        #region Category

        /// <summary>
        /// Есть ли такая категория в бд.
        /// </summary>
        public static bool HasCategory(string category)
        {
            return GetCategory(category) != null;
        }

        /// <summary>
        /// Возвращает category по name.
        /// </summary>
        public static Category GetCategory(string name)
        {
            return GetNewContext()?.Categories.FirstOrDefault(u => u.Name == name);
        }

        /// <summary>
        /// Добавить категорию
        /// </summary>
        public static void AddCategory(Category category)
        {
            // Добавить пользователя.
            _context.Categories.Add(category);

            // Сохранить изменения.
            SaveChanges();
        }

        /// <summary>
        /// Удалить категорию.
        /// </summary>
        public static void DeleteCategory(Category category)
        {
            _context.Categories.Remove(category);
            SaveChanges();
        }

        #endregion

        #region Image

        /// <summary>
        /// Есть ли такая картинка в бд.
        /// </summary>
        public static bool HasImage(string description)
        {
            return GetImage(description) != null;
        }

        /// <summary>
        /// Возвращает category по name.
        /// </summary>
        public static Image GetImage(string description)
        {
            return GetNewContext()?.Images.FirstOrDefault(u => u.Description == description);
        }

        /// <summary>
        /// Добавить категорию
        /// </summary>
        public static void AddImage(Image image)
        {
            // Добавить пользователя.
            _context.Images.Add(image);

            // Сохранить изменения.
            SaveChanges();
        }

        /// <summary>
        /// Удалить категорию.
        /// </summary>
        public static void DeleteImage(Image image)
        {
            _context.Images.Remove(image);
            SaveChanges();
        }

        #endregion

        /// <summary>
        /// Получить обновленный контекст
        /// </summary>
        public static SampleContext GetNewContext()
        {
            return new SampleContext();
        }

        /// <summary>
        /// Получить контекст
        /// </summary>
        public static SampleContext GetContext()
        {
            return _context;
        }

        /// <summary>
        /// Сохранить изменения.
        /// </summary>
        public static void SaveChanges()
        {
            try
            {
                _context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    MessageBox.Show(string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State));
                    foreach (var ve in eve.ValidationErrors)
                    {
                        MessageBox.Show(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage));
                    }
                }
                throw;
            }
            
        }
    }
}

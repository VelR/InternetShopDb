﻿using CodeFirst.Model;
using InternetShop.View;
using InternetShop.View.AdminViewComponents;
using InternetShop.ViewModel;
using System.Windows;

namespace InternetShop.Services
{
    /// <summary>
    /// Основная логика работы приложения.
    /// </summary>
    public static class AppService
    {
        #region Поля

        #endregion

        #region Свойства

        /// <summary>
        /// Авторизованный сотрудник
        /// </summary>
        public static Employee Employee { get; private set; }

        #endregion

        #region Методы

        /// <summary>
        /// Запуск приложения.
        /// </summary>        
        public static void StartApp()
        {
            Application.Current.MainWindow = new MainWindow();
            var dbService = new DataBaseService();

            ShowAdminView();

            if (Login())
            {
                ShowAdminView();
            }
        }

        /// <summary>
        /// Авторизация пользователя.
        /// </summary>
        private static bool Login()
        {
            var view = new LoginView();
            var loginViewModel = new LoginViewModel(view);
            view.DataContext = loginViewModel;
            if ((bool)view.ShowDialog())
            {
                Employee = loginViewModel.Employee;
                MessageBox.Show($"Авторизация прошла успешно!\n\rДобро пожаловать, {Employee.Name}!");
            }
            view.Close();

            return (bool)view.DialogResult;
        }

        /// <summary>
        /// Получить хэш пароля.
        /// </summary>
        public static int GetPasswordHash(string login, string password)
        {
            return (login.GetHashCode().ToString() + password.GetHashCode().ToString()).GetHashCode();
        }

        /// <summary>
        /// Показать окно админа
        /// </summary>
        private static void ShowAdminView()
        {
            var v = new AdminView { DataContext = new AdminViewModel() };
            v.ShowDialog();
        }

        #endregion
    }
}

﻿using CodeFirst;
using InternetShop.Services;
using System;
using System.Data.Entity;
using System.Windows;

namespace InternetShop
{
    /// <summary>
    /// Логика взаимодействия для App.xaml.
    /// </summary>
    public partial class App : Application
    {
        /// <summary>
        /// Стартер приложения.
        /// </summary>
        private void AppOnStartup(object sender, StartupEventArgs e)
        {
            try
            {                
                AppService.StartApp();
            }
            catch (Exception exception)
            {
                MessageBox.Show("Приложение будет закрыто." + Environment.NewLine +  exception.Message, "Критическая ошибка");
                throw;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using CodeFirst;
using CodeFirst.Model;

namespace InternetShop.View.MainViewComponents
{
    /// <summary>
    /// Interaction logic for AdminControl.xaml
    /// </summary>
    public partial class AdminViewControl : UserControl
    {
        //#region Поля

        //private SampleContext _context;

        //#endregion

        //#region Свойства

        ///// <summary>
        ///// Список продуктов (Биндиться к DataGrid)
        ///// </summary>
        //public List<Product> Products
        //{
        //    get { return (List<Product>)GetValue(ProductsProperty); }
        //    set { SetValue(ProductsProperty, value); }
        //}

        //public static readonly DependencyProperty ProductsProperty =
        //    DependencyProperty.Register("Products", typeof(List<Product>), typeof(AdminViewControl), new PropertyMetadata(null));

        //#endregion

        #region Конструктор
        
        public AdminViewControl(SampleContext context)
        {
            InitializeComponent();

            //_context = context;
            //Products = _context.Products.ToList();
            
            //ChangeButton.Click += ChangeButtonOnClick;
            //AddButton.Click += AddButtonOnClick;
            //DeleteButton.Click += DeleteButtonOnClick;
        }

        #endregion

        //#region Методы

        //private void DeleteProduct()
        //{
        //    var delIndex = ProductsDataGrid.SelectedIndex;
        //    _context.Products.Remove(Products[delIndex]);
        //    RefreshContext();
        //}
        //private void SaveChange()
        //{
        //    _context.SaveChanges();
        //}

        //#endregion

        //#region Обработчики кликов

        //private void SaveButtonOnClick(object sender, RoutedEventArgs routedEventArgs)
        //{
        //    SaveChange();
        //}

        //private void DeleteButtonOnClick(object sender, RoutedEventArgs routedEventArgs)
        //{
        //    DeleteProduct();
        //}

        //private void AddButtonOnClick(object sender, RoutedEventArgs routedEventArgs)
        //{
        //    var view = new ProductView(_context, null);
        //    view.Closed += ViewOnClosed;
        //    view.ShowDialog();
        //}

        //private void ChangeButtonOnClick(object sender, RoutedEventArgs routedEventArgs)
        //{
        //    if (ProductsDataGrid.SelectedItem != null)
        //    {
        //        var selectedProduct = _context.Products.ToList()[ProductsDataGrid.SelectedIndex];
        //        var view = new ProductView(_context, selectedProduct);
        //        view.Closed += ViewOnClosed;
        //        view.ShowDialog();
        //    }
        //}

        //private void RefreshContext()
        //{
        //    _context.SaveChanges();
        //    Products = _context.Products.ToList();
        //}

        ///// <summary>
        ///// Добавление товара
        ///// </summary>
        //private void AddProduct(Product product)
        //{
        //    _context.Products.Add(product);
        //    _context.SaveChanges();
        //}

        ///// <summary>
        ///// Изменение товара
        ///// </summary>
        //private void ChangeProduct(Product product)
        //{
        //    var newProduct = _context.Products.FirstOrDefault(p => p.ProductId == product.ProductId);
        //    if (newProduct != null)
        //    {
        //        newProduct.Name = product.Name;
        //        newProduct.Category = product.Category;
        //        newProduct.PurchasePrice = product.PurchasePrice;
        //        newProduct.Description = product.Description;
        //    }
        //    _context.SaveChanges();
        //}

        //#endregion
    }
}

﻿using InternetShop.ViewModel.AdminViewModelComponents;
using System.Windows.Controls;

namespace InternetShop.View.AdminViewComponents
{
    /// <summary>
    /// Логика взаимодействия для Orders.xaml
    /// </summary>
    public partial class Orders : UserControl
    {
        public Orders()
        {
            InitializeComponent();
            DataContext = new OrdersViewModel();
        }
    }
}

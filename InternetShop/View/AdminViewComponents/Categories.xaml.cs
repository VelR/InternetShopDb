﻿using InternetShop.ViewModel.AdminViewModelComponents;
using System.Windows.Controls;

namespace InternetShop.View.AdminViewComponents
{
    /// <summary>
    /// Логика взаимодействия для Categories.xaml
    /// </summary>
    public partial class Categories : UserControl
    {
        public Categories()
        {
            InitializeComponent();
            DataContext = new CategoriesViewModel();
        }
    }
}

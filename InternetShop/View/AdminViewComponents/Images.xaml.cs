﻿using InternetShop.ViewModel;
using InternetShop.ViewModel.AdminViewModelComponents;
using System.Windows.Controls;

namespace InternetShop.View.AdminViewComponents
{
    /// <summary>
    /// Логика взаимодействия для Images.xaml
    /// </summary>
    public partial class Images : UserControl
    {
        public Images()
        {
            InitializeComponent();
            DataContext = new ImagesViewModel();
        }
    }
}

﻿using InternetShop.ViewModel.AdminViewModelComponents;
using System.Windows.Controls;

namespace InternetShop.View.AdminViewComponents
{
    /// <summary>
    /// Логика взаимодействия для Users.xaml
    /// </summary>
    public partial class Products : UserControl
    {
        public Products()
        {
            InitializeComponent();
            DataContext = new ProductsViewModel();
        }
    }
}

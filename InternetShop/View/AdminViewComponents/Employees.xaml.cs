﻿using InternetShop.ViewModel.AdminViewModelComponents;
using System.Windows.Controls;

namespace InternetShop.View.AdminViewComponents
{
    /// <summary>
    /// Логика взаимодействия для Employees.xaml
    /// </summary>
    public partial class Employees : UserControl
    {
        public Employees()
        {
            InitializeComponent();
            DataContext = new EmployeesViewModel();
        }
    }
}
